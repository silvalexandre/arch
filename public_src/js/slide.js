(function() {
	$(document).ready(function() {
		$('.testimonials').owlCarousel({
			loop: true,
			margin: 10,
			nav: true,
			navText: ['< Anterior', 'Próximo >'],
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 2
				}
			}
		});
		$('.firstIntro').owlCarousel({
			loop: true,
			nav: false,
			items: 1,
			autoplay: true
		});
	});
})();