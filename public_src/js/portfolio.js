(function() {
	var loadMore = document.getElementById('loadMore');
	var jobs = document.getElementById('jobs');
	var projectNumber = 7;
	var categories = ['city', 'food', 'sports', 'nightlife', 'abstract', 'nature'];
	loadMore.addEventListener('click', loadMoreItems);

	function loadMoreItems() {
		createItem();
		createItem();
		createItem();
	}

	function createItem() {
		var item = document.createElement('div');
		var title = document.createElement('div');
		var h3 = document.createElement('h3');
		var projectTitle = document.createTextNode('Projeto ' + projectNumber);
		var small = document.createElement('small');
		var space = document.createTextNode(' ');
		var link = document.createElement('a');
		var linkText = document.createTextNode('ver mais');
		item.classList.add('item');
		item.style.backgroundImage = "url('http://lorempixel.com/370/280/" + categories[Math.floor(Math.random() * categories.length)] + "/')";
		link.href = "item.html";
		link.appendChild(linkText);
		small.appendChild(space);
		small.appendChild(link);
		h3.appendChild(projectTitle);
		h3.appendChild(small);
		title.classList.add('title');
		title.appendChild(h3);
		item.appendChild(title);
		jobs.appendChild(item);
		++projectNumber;
	}
})();